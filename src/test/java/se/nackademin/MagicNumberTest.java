package se.nackademin;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MagicNumberTest {
    MagicNumbers magicNumbers;

    @Before
    public void setUp() {
        magicNumbers = new MagicNumbers();
        magicNumbers.setAge(16);
        magicNumbers.setHeight(165);
        magicNumbers.setIncome(11000);
        magicNumbers.setLocation("Hagfors");
        magicNumbers.setName("Sture Hagfors");
    }

    @Test
    public void testA() {
        assertEquals(4, magicNumbers.calculateA());
    }
    
    @Test
    public void testAResultGreaterThan9() {       
        magicNumbers.setAge(9);
        magicNumbers.setName("Rafael");
        assertEquals(true, (magicNumbers.calculateA()> 9 || magicNumbers.calculateA()< 0));
    }

    @Test
    public void testAResultLessThan0() {       
        magicNumbers.setAge(-10);
        magicNumbers.setName("Rafael");
        //System.out.println("RESULT OF CALCULATE A = "+magicNumbers.calculateA());
        //assertEquals(true, (magicNumbers.calculateA()> 9 || magicNumbers.calculateA()< 0));
    }
    
    @Test
    public void testB() {
        assertEquals(3, magicNumbers.calculateB());
    }

    @Test
    public void testC() {
        assertEquals(2, magicNumbers.calculateC());
    }

    @Test
    public void testD() {
        assertEquals(6, magicNumbers.calculateD());
    }

    @Test
    public void testD2() {       
        magicNumbers.setAge(6);
        magicNumbers.setName("Rafael");
        assertEquals(0, magicNumbers.calculateD());
    }
    
     @Test
    public void testD3() {       
        magicNumbers.setAge(1);
        magicNumbers.setName("Rafael");
        assertEquals(2, magicNumbers.calculateD());
    }
    
    @Test
    public void testDWithAGreaterThan5() {
        magicNumbers.setAge(18);
        assertEquals(9, magicNumbers.calculateD());
    }

    @Test
    public void testE() {
        assertEquals(9, magicNumbers.calculateE());
    }
    @Test
    public void testEResultGreaterThan9() {       
        magicNumbers.setAge(1);
        magicNumbers.setIncome(4);
        magicNumbers.setHeight(6);
        assertEquals(true, (magicNumbers.calculateE()> 9 || magicNumbers.calculateE()< 0));
        
        magicNumbers.setAge(1);
        magicNumbers.setIncome(7);
        magicNumbers.setHeight(2);
        assertEquals(true, (magicNumbers.calculateE()> 9 || magicNumbers.calculateE()< 0));
        
        magicNumbers.setAge(2);
        magicNumbers.setIncome(4);
        magicNumbers.setHeight(3);
        assertEquals(true, (magicNumbers.calculateE()> 9 || magicNumbers.calculateE()< 0));
    }
    
     @Test
    public void testEResultLessThan0() {       
        magicNumbers.setAge(-50);
        magicNumbers.setIncome(-50);
        magicNumbers.setHeight(-50);
        //assertEquals(true, (magicNumbers.calculateE()> 9 || magicNumbers.calculateE()< 0));
        
    }
    

    @Test
    public void testADependsOnName() {
        int first = magicNumbers.calculateA();
        magicNumbers.setName("Sture i Hagfors");
        int second = magicNumbers.calculateA();
        assertNotEquals("A should provide different values for different names", first, second);
    }

    @Test
    public void testADependsOnAge() {
        int first = magicNumbers.calculateA();
        magicNumbers.setAge(18);
        int second = magicNumbers.calculateA();
        assertNotEquals("A should provide different values for different ages", first, second);
    }

    @Test
    public void testBDependsOnLocation() {
        int first = magicNumbers.calculateB();
        magicNumbers.setLocation("Grytevik");
        int second = magicNumbers.calculateB();
        assertNotEquals("B should provide different values for different locations", first, second);
    }

    @Test
    public void testBDependsOnIncome() {
        int first = magicNumbers.calculateB();
        magicNumbers.setIncome(219487);
        int second = magicNumbers.calculateB();
        assertNotEquals("B should provide different values for different income values", first, second);
    }

    @Test
    public void testCDependsOnHeight() {
        int first = magicNumbers.calculateC();
        magicNumbers.setHeight(126);
        int second = magicNumbers.calculateC();
        assertNotEquals("C should provide different values for different heights", first, second);
    }

    @Test
    public void testCDependsOnAge() {
        int first = magicNumbers.calculateC();
        magicNumbers.setAge(126);
        int second = magicNumbers.calculateC();
        assertNotEquals("C should provide different values for different ages", first, second);
    }

    @Test
    public void testCDependsOnName() {
        int first = magicNumbers.calculateC();
        magicNumbers.setName("Sture i Hagfors");
        int second = magicNumbers.calculateC();
        assertNotEquals("C should provide different values for different names", first, second);
    }
}

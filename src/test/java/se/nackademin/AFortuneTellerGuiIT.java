package se.nackademin;

import java.awt.event.ActionEvent;
import javax.swing.JButton;
import org.junit.Test;
import se.nackademin.gui.FortuneTellerGui;

public class AFortuneTellerGuiIT {
    FortuneTeller fortuneTeller = new FortuneTeller(new MagicNumbers(), new Translator());;
    FortuneTellerGui fortuneTellerGui = new FortuneTellerGui(fortuneTeller);
    

    @Test(timeout = 10000)
    public void testGetFortuneUsingGuiWrong() {
        ActionEvent actionEvent = new ActionEvent(new JButton(),0,"");        
        fortuneTellerGui.actionPerformed(actionEvent);
        
    }
       
    
    
}

